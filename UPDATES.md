# UPDATES

from `0.1.0` to `0.2.0`

1. Add  `build` function to wrap the app.json for better testing
2. move options to index.js

```
{
  logger: true,
  trustProxy: true
}
```
3. On open-api.js, used package.json to determine the open-api docs title, description, and version
4. Updated models/index.js
5. Add utils/schema to transform schema
6. Updated and install yaml-loader to allow usage of yaml files
7.
