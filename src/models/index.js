/**
 * @module
 * @description Model Index
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { processDefinitions } from '../utils/process-definitions';
// import Name from './definitions/atomic/name.json';
// import UserData from './definitions/data/user.json';
// import UserPostRequest from './definitions/request/user-post.json';
// import UserPutRequest from './definitions/request/user-put.json';
// import UserPutOneRequest from './definitions/request/user-put-one.json';
// import UserDeleteRequest from './definitions/request/user-delete.json';
// import UserList from './definitions/data/user-list.json';
import Date from './definitions/atomic/date.json';
import Summary from './definitions/atomic/summary.json';
import Text from './definitions/atomic/text.json';
import Title from './definitions/atomic/title.json';
import UID from './definitions/atomic/uid.json';
import ID from './definitions/atomic/id.json';
import BlogList from './definitions/data/blog-list.json';
import BlogSummary from './definitions/data/blog-summary.json';
import Blog from './definitions/data/blog.json';
import BlogPost from './definitions/request/blog-post.json';
import BlogPutOne from './definitions/request/blog-put-one.json';
import BlogListResponse from './definitions/response/blog-list-response.json';
import BlogResponse from './definitions/response/blog-response.json';
import Success from './definitions/atomic/success.json';
import SuccessfulResponse from './definitions/response/successful-response.json';

/** @type {*} */
const preprocessedDef = {
  Date,
  UID,
  ID,
  Success,
  Summary,
  Text,
  Title,
  BlogList,
  BlogSummary,
  Blog,
  BlogPost,
  BlogPutOne,
  BlogListResponse,
  BlogResponse,
  SuccessfulResponse
};

const definitions = processDefinitions(preprocessedDef);

export {
  definitions
};
