/**
 * @module
 * @description The Fastify app
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Fastify from 'fastify';
import fastifySwagger from 'fastify-swagger';
import { swagger } from './open-api';
import { api } from './api';
import { errorHandler } from './utils/error';

export const build = async (opts = {}) => {
  const fastify = Fastify(opts);

  // @ts-ignore
  fastify.register(require('fastify-cors'), {
    origin: true,
    credentials: true
  });

  fastify
    // @ts-ignore
    .register(require('fastify-sensible'))
    .after(() => {
      // @ts-ignore
      return fastify.setErrorHandler(errorHandler);
    });

  // @ts-ignore
  fastify.register(fastifySwagger, {
    routePrefix: '/docs',
    swagger,
    exposeRoute: process.env.NODE_ENV === 'development' || !process.env.NODE_ENV
  });

  for (const models in swagger.definitions) {
    fastify.addSchema(swagger.definitions[models]);
  }

  for (const key in api) {
    for (const path in api[key]) {
      for (const method in api[key][path]) {
        const { schema } = api[key][path][method];
        /* istanbul ignore else */
        // @ts-ignore
        if (fastify[method] && typeof fastify[method] === 'function') {
          /* istanbul ignore next */
          // @ts-ignore
          fastify[method](`${schema.api ? `/api${path}` : path}`, api[key][path][method]);
        }
      }
    }
  }

  return fastify;
};
