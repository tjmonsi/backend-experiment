import configEnv from '../../config.json';
const { environment } = /** @type {ConfigEnv} */(/** @type {*} */(configEnv));
const { common } = environment;
const env = (/** @type {*} */(environment[process.env.ENV || 'development']));

export {
  env,
  common
};
