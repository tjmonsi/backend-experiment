import { admin } from '../admin';

/**
 *
 * @param {import("fastify").FastifyRequest} request
 * @param {import("fastify").FastifyReply<Response>} reply
 */
export const authPreHandler = async (request, reply) => {
  const { headers } = request;

  const [, token] = (headers && headers.authorization && headers.authorization.split('Bearer ')) || [null, null];

  if (!token) {
    reply.unauthorized('auth/forbidden');
    return null;
  }

  try {
    const decodedToken = await admin.auth().verifyIdToken(token);

    request.body = request.body || {};

    // @ts-ignore
    request.body.user = decodedToken;
  } catch (error) {
    console.error(error);
    reply.unauthorized(error.message);
  }

  return null;
};

/**
 *
 * @param {import("fastify").FastifyRequest} request
 * @param {import("fastify").FastifyReply<Response>} reply
 */
export const authIsOwner = async (request, reply) => {
  const { query } = request;
  const { isOwner } = /** @type {*} */(query);

  if (isOwner === 'true') {
    return authPreHandler(request, reply);
  }

  return null;
};
