import { definitions } from '../../models';

/**
 *
 * @param {*} schema
 */
export const transformerSchema = (schema) => {
  for (const key in schema) {
    if (typeof schema[key] === 'string' && schema[key].match(/^[a-z|A-Z|0-9]+#{1}$/g) && definitions[schema[key].split('#')[0]]) {
      schema[key] = definitions[schema[key].split('#')[0]];
    } else if (typeof schema[key] === 'object' && schema[key] && !(schema[key] instanceof Array)) {
      transformerSchema(schema[key]);
    }
  }

  return schema;
};
