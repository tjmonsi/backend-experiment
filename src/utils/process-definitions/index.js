/**
 * @module
 * @description Process the Definitions
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * @param {SwaggerModelDefinition} preprocessedDef
 */
export const processDefinitions = (preprocessedDef) => {
  /** @type {*} */
  const idMap = {};

  for (const i in preprocessedDef) {
    const definition = preprocessedDef[i];
    if (!definition.$id) definition.$id = i;
    const { $id } = definition;
    idMap[`${$id}#`] = definition;
  }

  /**
   *
   * @param {*} obj
   */
  const processObject = (obj) => {
    for (const key in obj) {
      const strObj = obj[key];

      if (typeof strObj === 'string') {
        const $id = strObj;

        // show error if an id map wasn't defined
        if (!idMap[$id]) {
          console.error(`There's no data definition for ${$id}`);
        }

        obj[key] = {};

        for (const i in idMap[$id]) {
          if (i !== '$id') {
            obj[key][i] = idMap[$id][i];
          }
        }
      } else if (typeof strObj === 'object' && strObj) {
        if (strObj.type === 'array') {
          const $id = strObj.items;

          // show error if an id map wasn't defined
          if (!idMap[$id]) {
            console.error(`There's no data definition for ${$id}`);
          }

          strObj.items = {};

          for (const i in idMap[$id]) {
            if (i !== '$id') {
              strObj.items[i] = idMap[$id][i];
            }
          }
          // strObj.items = idMap[$id];
        } else if (strObj.type === 'object') {
          processObject(strObj.properties);
        }
      }
    }
  };

  for (const i in idMap) {
    if (idMap[i].type === 'array') {
      const $id = idMap[i].items;

      // show error if an id map wasn't defined
      if (!idMap[$id]) {
        console.error(`There's no data definition for ${$id}`);
      }

      idMap[i].items = {};

      for (const j in idMap[$id]) {
        if (j !== '$id') {
          idMap[i].items[j] = idMap[$id][j];
        }
      }
      // idMap[i].items = idMap[$id];
    } else if (idMap[i].type === 'object') {
      processObject(idMap[i].properties);
    }
  }

  /**
  * @type {SwaggerModelDefinition}
  */
  const definitions = {};

  for (const i in idMap) {
    definitions[i.split('#')[0]] = idMap[i];
  }

  return definitions;
};
