/**
 * @module
 * @description Secrets maanager module
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SecretManagerServiceClient } from '@google-cloud/secret-manager';
import { env, common } from '../config';
const { keys: envKeys, projectId } = env.google;
const { keys: commonKeys } = common.google;

const keys = [...commonKeys, ...envKeys];

const secretManagerClient = new SecretManagerServiceClient();

/** @type {Config} */
const config = {
  initialized: false
};

export const init = async () => {
  const promises = [];

  for (const keyVersion of keys) {
    const [key, version = 'latest'] = keyVersion.split(':');

    promises.push(secretManagerClient.accessSecretVersion({
      name: `projects/${projectId}/secrets/${key}/versions/${version}`
    }));
  }

  const results = await Promise.allSettled(promises);

  for (const index in results) {
    const { status, value, reason } = /** @type {{ status: string, value: *, reason: * }} */(results[index]);
    if (status === 'fulfilled') {
      const [accessResponse] = value;
      const key = keys[index];

      config[key] = accessResponse &&
        accessResponse.payload &&
        accessResponse.payload.data &&
        accessResponse.payload.data.toString();
    } else if (status === 'rejected') {
      console.error(reason);
    }
  }

  config.initialized = true;

  return config;
};

export const getSecrets = async () => {
  if (!config.initialized) {
    return init();
  }
  return config;
};
