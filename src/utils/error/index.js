/**
 * @module
 * @description The error handler
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import config from '../../config.json';
const { errors: additionalErrors } = config;

/**
 *
 * @param {*} error
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply<import('http').ServerResponse>} reply
 */
/* istanbul ignore next */
export const errorHandler = (error, request, reply) => {
  const statusCode = error.statusCode || 500;

  if (statusCode >= 500) {
    request.log.error(error);
  } else if (statusCode >= 400) {
    request.log.info(error);
  } else {
    request.log.error(error);
  }

  const generalErrors = {
    'request/malformed': 'The request could not be understood by the server due to malformed syntax. ' +
        'The client SHOULD NOT repeat the request without modifications.',
    'auth/unauthorized': 'The request requires user authentication. ' +
        'The client MAY repeat the request with a suitable Authorization header field. ' +
        'If the request already included Authorization credentials, ' +
        'then the 401 response indicates that authorization has been refused for those credentials.',
    'auth/forbidden': 'The server understood the request, ' +
        'but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.',
    'data/not-found': 'The data you are trying to find cannot be found.',
    'request/conflict': 'The request could not be completed due to a conflict with the current state of the resource. ' +
        'This code is only allowed in situations where it is expected that ' +
        'the user might be able to resolve the conflict and resubmit the request.',
    'request/rate-limit': 'The user has sent too many requests in a given amount of time ("rate limiting")',
    'server/general-error': 'The server encountered an unexpected condition which prevented it from fulfilling the request.',
    'server/unavailable': 'The server is currently unable to handle ' +
        'the request due to a temporary overloading or maintenance of the server.',
    'request/unsupported-media-type': 'The server is refusing to service the request ' +
        'because the entity of the request is in a format not supported by the requested resource for the requested method.',
    'request/too-large': 'The server is refusing to process a request because ' +
        'the request entity is larger than the server is willing or able to process.'
  };

  /** @type {Object<string, string>} */
  const errors = {
    ...generalErrors,
    ...additionalErrors
  };

  /* istanbul ignore next */
  reply
    .code(statusCode)
    .send({
      success: false,
      code: errors[error.message] ? error.message : 'server/general-error',
      message: errors[error.message] || error.message
    });
};
