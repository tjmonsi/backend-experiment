/**
 * @module
 * @description Sentry module
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as Sentry from '@sentry/node';
import { getSecrets } from '../secret';
import packageConfig from '../../../package.json';

let initialize = false;

/**
 * @return {Promise<Sentry>}
 */
const init = async () => {
  const release = `${packageConfig.name}@${packageConfig.version}`;
  console.log(release);

  const { sentry: dsn } = await getSecrets();

  try {
    if (dsn) {
      Sentry.init({
        dsn: /** @type {string} */(dsn),
        release
      });
    }
  } catch (error) {
    console.error(error);
  }

  initialize = true;
  return Sentry;
};

/**
 * @return {Promise<Sentry>}
 */
export const getSentry = async () => {
  if (!initialize) {
    return init();
  }
  return Sentry;
};
