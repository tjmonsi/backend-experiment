import { Firestore, FieldValue } from '@google-cloud/firestore';

// Create a new client
export const firestore = new Firestore();
export {
  FieldValue
};
