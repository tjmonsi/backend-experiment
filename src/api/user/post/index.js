/**
 * @module
 * @description Get method for the user
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import schema from './schema.yml';
import { handler } from './handler.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';
import { transformerSchema } from '../../../utils/schema/transformer-schema.js';

const newSchema = transformerSchema(schema);

export const opts = {
  schema: {
    ...newSchema,
    response: {
      ...newSchema.response,
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  handler
};
