/**
 * @module
 * @description The controller function to handle the Get method for User
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { firestore } from '../../../utils/firestore';

/**
 *
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply<Response>} response
 */
export const handler = async (request, response) => {
  const { query } = request;
  const { limit = 2, nextId, prevId } = /** @type {*} */(query || {});
  // console.log(body);
  const collectionRef = firestore.collection('blog');
  let queryRef = collectionRef
    .orderBy('dateCreated', 'desc')
    .limit(limit);

  if (nextId) {
    const last = await collectionRef.doc(nextId).get();
    const next = last.data();
    if (next) {
      queryRef = queryRef.startAfter(next.dateCreated);
    }
  }

  if (prevId) {
    const first = await collectionRef.doc(prevId).get();
    const prev = first.data();
    if (prev) {
      queryRef = queryRef.endBefore(prev.dateCreated);
    }
  }

  const snapshot = await queryRef.get();

  const data = [];

  for (const doc of snapshot.docs) {
    const { title, summary, owner, dateCreated, dateUpdated } = doc.data();
    data.push({
      id: doc.id,
      title,
      summary,
      owner,
      dateCreated: dateCreated.toMillis(),
      dateUpdated: dateUpdated.toMillis()
    });
  }

  if (data) {
    return {
      success: true,
      data
    };
  }

  return response.notFound('No data');
};
