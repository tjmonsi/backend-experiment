/**
 * @module
 * @description The controller function to handle the Get method for User
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { firestore } from '../../../utils/firestore';

/**
 *
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply<Response>} response
 */
export const handler = async (request, response) => {
  const { params, query } = request;
  const { isOwner } = /** @type {*} */(query);
  const { id } = /** @type {*} */(params);
  // console.log(body);
  const doc = firestore.collection('blog').doc(id);

  const dataRef = await doc.get();
  const data = dataRef.data();

  // checking if it is the owner
  if (isOwner === 'true') {
    const { body } = request;
    const { user } = /** @type {*} */(body) || {};
    if (user) {
      const { uid } = user;
      console.log(uid);
      if (data && data.owner === uid) {
        return {
          success: true,
          data: {
            ...data,
            dateCreated: data.dateCreated.toMillis(),
            dateUpdated: data.dateUpdated.toMillis(),
            id: doc.id
          }
        };
      }
    }

    response.forbidden('auth/forbidden');
  }

  if (data) {
    return {
      success: true,
      data: {
        ...data,
        dateCreated: data.dateCreated.toMillis(),
        dateUpdated: data.dateUpdated.toMillis(),
        id: doc.id
      }
    };
  }

  return response.notFound('Data was not saved');
};
