/**
 * @module
 * @description The controller function to handle the Get method for User
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { firestore, FieldValue } from '../../../utils/firestore';

/**
 *
 * @param {import('fastify').FastifyRequest} request
 * @param {import('fastify').FastifyReply<Response>} response
 */
export const handler = async (request, response) => {
  const { body } = request;
  const { title, text, summary } = /** @type {*} */(body);
  const doc = firestore.collection('blog').doc();

  const { user } = /** @type {*} */(body) || {};
  if (user) {
    const { uid } = user;
    await doc.set({
      title,
      text,
      summary,
      owner: uid,
      dateCreated: FieldValue.serverTimestamp(),
      dateUpdated: FieldValue.serverTimestamp()
    });

    const dataRef = await doc.get();
    const data = dataRef.data();

    if (data) {
      return {
        success: true,
        data: {
          ...data,
          dateCreated: data.dateCreated.toMillis(),
          dateUpdated: data.dateUpdated.toMillis(),
          id: doc.id
        }
      };
    }

    return response.notFound('Data was not saved');
  }
};
