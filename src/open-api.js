/**
 * @module
 * @description Open API configuration
 *
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { definitions } from './models';
import securityDefinitions from './security.yml';
import packageJson from '../package.json';
const { name: title, description, version } = packageJson;

const schemes = process.env.NODE_ENV !== 'production'
  ? ['http', 'https']
  /* istanbul ignore next */
  : ['https'];

export const swagger = {
  info: {
    title,
    description,
    version
  },
  schemes,
  consumes: ['application/json'],
  produces: ['application/json'],
  definitions,
  securityDefinitions
};
