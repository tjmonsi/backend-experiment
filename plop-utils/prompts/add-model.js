/**
 * @module
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @returns {Array<*>}
 */
module.exports = () => [
  {
    type: 'input',
    name: 'name',
    message: 'Put the name of the model',
    /**
     * @ignore
     * @param {string} value
     * @return {string | boolean}
     */
    validate: value => {
      if ((/.+/).test(value) && value.indexOf('/') < 0) { return true; }
      return 'Name is required';
    }
  },
  {
    type: 'input',
    name: 'description',
    message: 'What is this model for?',
    default: ''
  },
  {
    type: 'list',
    name: 'modelType',
    message: 'What is this model? A response or just a data type',
    default: 'data',
    choices: [
      'data',
      'request',
      'response'
    ]
  },
  {
    type: 'list',
    name: 'dataType',
    message: 'Data type of this model',
    default: 'string',
    choices: [
      'string',
      'number',
      'boolean',
      'object',
      'array'
    ]
  }
];
