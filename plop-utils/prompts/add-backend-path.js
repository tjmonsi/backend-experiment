/**
 * @module
 * @license
 * Copyright 2020, Senti Techlabs Inc..
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @returns {Array<*>}
 */
module.exports = () => [
  {
    type: 'input',
    name: 'name',
    message: 'Put the name of the path',
    /**
     * @ignore
     * @param {string} value
     * @return {string | boolean}
     */
    validate: value => {
      if ((/.+/).test(value) && value.indexOf('/') < 0) { return true; }
      return 'Name is required';
    }
  },
  {
    type: 'input',
    name: 'pathName',
    message: 'Add path if needed (i.e. -> [pathname]/[path] or [pathname]/this/path or [pathname]/:param1/:param2)',
    default: ''
  },
  {
    type: 'confirm',
    name: 'isApi',
    message: 'Should we add a prefix of `api`? This is best to determine if this is an API',
    default: true
  },
  {
    type: 'list',
    name: 'method',
    message: 'What is the HTTP method for this?',
    choices: [
      'get',
      'post',
      'put',
      'delete',
      'patch'
    ]
  }
];
